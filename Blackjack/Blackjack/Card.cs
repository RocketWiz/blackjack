﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Blackjack
{
    class Card
    {
        int _num, _numValue;
        int _x = 10, _y = 10;
        string _shape;
        string _color;
        Image _img;
        PictureBox _pic;
        public Card(int num, int numValue, string shape, string color)
        {
            this._num = num;
            this._numValue = numValue;
            this._shape = shape;
            this._color = color;
            _img = (Image)Properties.Resources.ResourceManager.GetObject(shape + num);
            defpic();
        }
        private void defpic()
        {
            _pic = new PictureBox();
            _pic.Image = _img;
            _pic.Location = new Point(_x, _y);
            _pic.Size = new Size(200,250);
            _pic.SizeMode = PictureBoxSizeMode.StretchImage;
            _pic.Tag = _shape + _num;
            _pic.BorderStyle = BorderStyle.None;
        }
        public int Num { get { return _num; } set { _num = value; } }
        public int NumValue { get { return _numValue; } set { _numValue = value; } }
        public string Shape { get { return _shape; } set { _shape = value; } }
        public string Color { get { return _color; } set { _color = value; } }
        public Image Image { get { return _img; } set { _img = value; } }
        public PictureBox PictureBox { get { return _pic; } set { _pic = value; } }
        //public int X { get { return _x; } set { _x = value; } }
        //public int Y { get { return _y; } set { _y = value; } }
        public int GetX()
        {
            return _x;
        }
        public int GetY()
        {
            return _y;
        }
        
        public void SetXY(int x, int y)
        {
            this._x = x;
            this._y = y;
            _pic.Location = new Point(x, y);
        }
    }
}