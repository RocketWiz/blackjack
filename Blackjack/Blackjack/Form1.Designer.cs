﻿namespace Blackjack
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {            
            this.lblBet1 = new System.Windows.Forms.Label();
            this.lblBet10 = new System.Windows.Forms.Label();
            this.lblBet100 = new System.Windows.Forms.Label();
            this.lblBet500 = new System.Windows.Forms.Label();
            this.lblBetAmount = new System.Windows.Forms.Label();
            this.lblMoney = new System.Windows.Forms.Label();
            this.lblDeal = new System.Windows.Forms.Label();
            this.lblSum = new System.Windows.Forms.Label();
            this.lblHit = new System.Windows.Forms.Label();
            this.lblStand = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lblBet1
            // 
            this.lblBet1.BackColor = System.Drawing.Color.Transparent;
            this.lblBet1.Location = new System.Drawing.Point(12, 449);
            this.lblBet1.Name = "lblBet1";
            this.lblBet1.Size = new System.Drawing.Size(270, 234);
            this.lblBet1.TabIndex = 0;
            this.lblBet1.Click += new System.EventHandler(this.lblBet1_Click);
            // 
            // lblBet10
            // 
            this.lblBet10.BackColor = System.Drawing.Color.Transparent;
            this.lblBet10.Location = new System.Drawing.Point(2, 696);
            this.lblBet10.Name = "lblBet10";
            this.lblBet10.Size = new System.Drawing.Size(268, 201);
            this.lblBet10.TabIndex = 1;
            this.lblBet10.Click += new System.EventHandler(this.lblBet10_Click);
            // 
            // lblBet100
            // 
            this.lblBet100.BackColor = System.Drawing.Color.Transparent;
            this.lblBet100.Location = new System.Drawing.Point(160, 884);
            this.lblBet100.Name = "lblBet100";
            this.lblBet100.Size = new System.Drawing.Size(247, 180);
            this.lblBet100.TabIndex = 2;
            this.lblBet100.Click += new System.EventHandler(this.lblBet100_Click);
            // 
            // lblBet500
            // 
            this.lblBet500.BackColor = System.Drawing.Color.Transparent;
            this.lblBet500.Location = new System.Drawing.Point(413, 894);
            this.lblBet500.Name = "lblBet500";
            this.lblBet500.Size = new System.Drawing.Size(274, 170);
            this.lblBet500.TabIndex = 3;
            this.lblBet500.Click += new System.EventHandler(this.lblBet500_Click);
            // 
            // lblBetAmount
            // 
            this.lblBetAmount.AutoSize = true;
            this.lblBetAmount.BackColor = System.Drawing.Color.Transparent;
            this.lblBetAmount.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblBetAmount.ForeColor = System.Drawing.Color.White;
            this.lblBetAmount.Location = new System.Drawing.Point(840, 984);
            this.lblBetAmount.Name = "lblBetAmount";
            this.lblBetAmount.Size = new System.Drawing.Size(0, 55);
            this.lblBetAmount.TabIndex = 4;
            // 
            // lblMoney
            // 
            this.lblMoney.AutoSize = true;
            this.lblMoney.BackColor = System.Drawing.Color.Transparent;
            this.lblMoney.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMoney.ForeColor = System.Drawing.Color.White;
            this.lblMoney.Location = new System.Drawing.Point(100, 385);
            this.lblMoney.Name = "lblMoney";
            this.lblMoney.Size = new System.Drawing.Size(0, 55);
            this.lblMoney.TabIndex = 5;
            // 
            // lblDeal
            // 
            this.lblDeal.BackColor = System.Drawing.Color.Transparent;
            this.lblDeal.Location = new System.Drawing.Point(349, 675);
            this.lblDeal.Name = "lblDeal";
            this.lblDeal.Size = new System.Drawing.Size(174, 151);
            this.lblDeal.TabIndex = 6;
            this.lblDeal.Click += new System.EventHandler(this.lblDeal_Click);
            // 
            // lblSum
            // 
            this.lblSum.AutoSize = true;
            this.lblSum.BackColor = System.Drawing.Color.Transparent;
            this.lblSum.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSum.ForeColor = System.Drawing.Color.White;
            this.lblSum.Location = new System.Drawing.Point(1475, 690);
            this.lblSum.Name = "lblSum";
            this.lblSum.Size = new System.Drawing.Size(0, 55);
            this.lblSum.TabIndex = 7;
            // 
            // lblHit
            // 
            this.lblHit.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblHit.Location = new System.Drawing.Point(1399, 346);
            this.lblHit.Name = "lblHit";
            this.lblHit.Size = new System.Drawing.Size(200, 200);
            this.lblHit.TabIndex = 8;
            this.lblHit.Text = "Hit";
            this.lblHit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblHit.Click += new System.EventHandler(this.lblHit_Click);
            // 
            // lblStand
            // 
            this.lblStand.Font = new System.Drawing.Font("Microsoft Sans Serif", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblStand.Location = new System.Drawing.Point(1399, 814);
            this.lblStand.Name = "lblStand";
            this.lblStand.Size = new System.Drawing.Size(200, 200);
            this.lblStand.TabIndex = 9;
            this.lblStand.Text = "Stand";
            this.lblStand.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblStand.Click += new System.EventHandler(this.lblStand_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.BackgroundImage = global::Blackjack.Properties.Resources.ChooseYourBet;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(1920, 1061);
            this.Controls.Add(this.lblStand);
            this.Controls.Add(this.lblHit);
            this.Controls.Add(this.lblSum);
            this.Controls.Add(this.lblDeal);
            this.Controls.Add(this.lblMoney);
            this.Controls.Add(this.lblBetAmount);
            this.Controls.Add(this.lblBet500);
            this.Controls.Add(this.lblBet100);
            this.Controls.Add(this.lblBet10);
            this.Controls.Add(this.lblBet1);
            this.DoubleBuffered = true;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblBet1;
        private System.Windows.Forms.Label lblBet10;
        private System.Windows.Forms.Label lblBet100;
        private System.Windows.Forms.Label lblBet500;
        private System.Windows.Forms.Label lblBetAmount;
        private System.Windows.Forms.Label lblMoney;
        private System.Windows.Forms.Label lblDeal;
        private System.Windows.Forms.Label lblSum;
        private System.Windows.Forms.Label lblHit;
        private System.Windows.Forms.Label lblStand;
    }
}

