﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Blackjack
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        //Card c1 = new Card(7, "h", "red");
        //Controls.Add(c1.PictureBox);
        Player dealer, player1;
        Deck d;
        int money, betAmount = 0, playerSum = 0, dealerSum = 0;
        bool enableBets = true, enableDeal = true;

        private void lblBet1_Click(object sender, EventArgs e)
        {
            if(enableBets)
            {
                betAmount += 1;
                money -= 1;
                lblBetAmount.Text = betAmount + "";
                lblMoney.Text = money + "";
            }            
        }

        private void lblBet10_Click(object sender, EventArgs e)
        {
            if (enableBets)
            {
                betAmount += 10;
                money -= 10;
                lblBetAmount.Text = betAmount + "";
                lblMoney.Text = money + "";
            }
        }

        private void lblHit_Click(object sender, EventArgs e)
        {            
            int count = player1.MyHand.Count;
            for (int i = 0; i < count; i++)
            {
                player1.MyHand[i].SetXY(player1.MyHand[i].GetX() - 100, player1.Y);
                if(i + 1 == player1.MyHand.Count)
                {
                    player1.addCardToHand(d.takeCard());
                    player1.MyHand[i+1].SetXY(player1.MyHand[i].GetX() + 205 , player1.Y);
                    this.Controls.Add(player1.MyHand[i+1].PictureBox);
                    playerSum += player1.MyHand[i+1].NumValue;
                    lblSum.Text = playerSum + "";
                }
            }
            //CheckWin();
            if(playerSum >= 21)
            {
                Stand();
                dealer.MyHand[1].PictureBox.Image = dealer.MyHand[1].Image;
            }
        }

        //private void CheckWin();
        private void lblBet100_Click(object sender, EventArgs e)
        {
            if (enableBets)
            {
                betAmount += 100;
                money -= 100;
                lblBetAmount.Text = betAmount + "";
                lblMoney.Text = money + "";
            }
        }

        private void lblStand_Click(object sender, EventArgs e)
        {
            clearForm();
        }

        private void lblBet500_Click(object sender, EventArgs e)
        {
            if (enableBets)
            {
                betAmount += 500;
                money -= 500;
                lblBetAmount.Text = betAmount + "";
                lblMoney.Text = money + "";
            }
        }

        private void lblDeal_Click(object sender, EventArgs e)
        {
            if(enableDeal)
            {
                if (betAmount != 0)
                {
                    this.BackgroundImage = Properties.Resources.Ingame_Screen;
                    drawHands();
                    enableDeal = false;
                }
                else
                    MessageBox.Show("Please choose your bet amount first");
            }           
        }

        private void Form1_Load(object sender, EventArgs e)
        {          
            d = new Deck(this);
            dealer = new Player("Jack", 10000, 750, 250);
            player1 = new Player("Noam", 1000, 750, 600);
            money = player1.Money;
            lblMoney.Text = money + "";
            //this.BackgroundImage = Properties.Resources.ChooseYourBet;
            //this.BackgroundImageLayout = ImageLayout.Stretch;
        }

        private void drawHands()
        {
            player1.addCardToHand(d.takeCard());
            player1.addCardToHand(d.takeCard());
            player1.showHand(this, false);
            dealer.addCardToHand(d.takeCard());
            dealer.addCardToHand(d.takeCard());
            dealer.showHand(this, true);
            enableBets = false;
            playerSum = player1.MyHand[0].NumValue + player1.MyHand[1].NumValue;
            dealerSum = dealer.MyHand[0].NumValue + dealer.MyHand[1].NumValue;
            lblSum.Text = playerSum + "";
        }

        private void Stand()
        {
            while (dealerSum < 17)
            {
                int c = dealer.MyHand.Count;
                for (int i = 0; i < c; i++)
                {
                    dealer.MyHand[i].SetXY(dealer.MyHand[i].GetX() - 100, dealer.Y);
                    if (i + 1 == dealer.MyHand.Count)
                    {
                        dealer.addCardToHand(d.takeCard());
                        dealer.MyHand[i + 1].SetXY(dealer.MyHand[i].GetX() + 205, dealer.Y);
                        this.Controls.Add(dealer.MyHand[i + 1].PictureBox);
                        dealerSum += dealer.MyHand[i + 1].NumValue;
                    }
                }
            }
        }

        public void clearForm()
        {
            for (int i = 0; i < player1.MyHand.Count; i++)
                this.Controls.Remove(player1.MyHand[i].PictureBox);               
            player1.MyHand.Clear();
            for (int i = 0; i < dealer.MyHand.Count; i++)
                this.Controls.Remove(dealer.MyHand[i].PictureBox);
            betAmount = 0;
            playerSum = 0;
            dealerSum = 0;
            enableBets = true;
            enableDeal = true;
            this.BackgroundImage = Properties.Resources.ChooseYourBet;
        }
    }
}