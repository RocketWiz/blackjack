﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blackjack
{
    class Player
    {
        Form1 frm;
        string name;
        int money;
        List<Card> myHand;
        int x, y;
        public Player(string name, int money, int x, int y)
        {
            this.name = name;
            this.money = money;
            myHand = new List<Card>();
            this.x = x;
            this.y = y;          
        }

        public int Money { get { return money; } set { money = value; } }
        public List<Card> MyHand { get { return myHand; } set { myHand = value; } }
        public int X { get { return x; } set { x = value; } }
        public int Y { get { return y; } set { y = value; } }
        public void addCardToHand(Card c)
        {
            myHand.Add(c);
        }
        public void showHand(Form1 frm, bool isDealer)
        { 
            for(int i = 0; i < myHand.Count; i++)
            {             
                myHand[i].SetXY(x, y);
                if (isDealer && i == 1)
                {
                    myHand[i].PictureBox.Image = Properties.Resources.Back;
                    frm.Controls.Add(myHand[i].PictureBox);
                }
                frm.Controls.Add(myHand[i].PictureBox);           
                x += 205;
            }
        }
        public void showHandMystery(Form1 frm)
        {
            myHand[1].Image = Properties.Resources.Back;
        }
    }
}
