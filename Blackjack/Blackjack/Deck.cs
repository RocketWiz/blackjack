﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blackjack
{
    class Deck
    {
        Form1 frm;
        string[] shapes = { "c", "d", "h", "s" };
        Card[] deck = new Card[52];
        Random rnd = new Random();
        int topCard = 0;
        public Deck(Form1 frm)
        {
            this.frm = frm;
            initDeck();
            shuffle();
            //showCardOnForm();
        }
        public Card takeCard()
        {
            return deck[topCard++];
        }
        public void initDeck()
        {
            //אתחול מערך הקלפים
            int numValue = 0;
            string color;           
            int k = 0;
            for (int i = 0; i < 4; i++)
            {
                for (int j = 1; j <= 13; j++)
                {
                    if (i == 0 || i == 3)
                        color = "black";
                    else
                        color = "red";
                    if (j > 10)
                        numValue = 10;
                    else
                        numValue = j;
                    deck[k] = new Card(j, numValue, shapes[i], color);
                    k++;
                }
            }
        }
        public void shuffle()
        {
            int k;
            Card tmp;
            for (int i = 0; i < deck.Length - 1; i++)
            {
                k = rnd.Next(i, deck.Length);
                tmp = deck[k];
                deck[k] = deck[i];
                deck[i] = tmp;
            }
        }
        public void showCardOnForm()
        {
            int x = 10, y = 10;
            int k = 0;
            for(int i = 1; i <= 4; i++)
            {
                deck[k].SetXY(x, y);
                for(int j = 1; j <= 13; j++)
                {
                    deck[k].SetXY(x, y);
                    x += 125;
                    frm.Controls.Add(deck[k++].PictureBox);
                }
                x = 10;
                y += 250;
            }           
        }
    }
}